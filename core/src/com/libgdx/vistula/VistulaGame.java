package com.libgdx.vistula;

//Import paczki Game oraz klasy SplashScreem
import com.badlogic.gdx.Game;
import com.libgdx.vistula.screens.SplashScreen;

// Pierwsza klasa która włącza się po uruchomieniu programu
public class VistulaGame extends Game {

	@Override
	public void create () {
// Utworzenie SplashScreena i ustawienie go jako aktywnego
		this.setScreen(new SplashScreen(this));
	}
// Wykonanie kodu który znajduje się w klasie Game w metodzie render()
	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
	}
}
