    package com.libgdx.vistula.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.libgdx.vistula.VistulaGame;

// Jest to klasa odpowiedzialana za wyświetlanie menu głównego.
public class MainMenu implements Screen {

    final VistulaGame game;
    SpriteBatch batch;
//    BitmapFont jest obiektem, który umożliwia pisanie tekstu za pomocą wybranej czcionki.
    BitmapFont font;
//    Stage jest obiektem, który wyświetla elementy UI na ekranie, w tym przypadku jest to menu główne.
    Stage stage;
//    Skin jest obiektem, który przechowuje informacje o wygladzię danego typu elementu.
    Skin skin;
//    Sound jest obiektem dźwiękowym, który jest ładowany do pamięci urządzenia i odtwarzany z niej w razie potrzeby.
    Sound click;
//    Dla dłuższych i większych dźwięków zaleca się korzystanie z obiektu Music, który działa na zasadzie streamowania muzyki z urządzenia.
    Music bgMusic;
    Texture img;
//    Zmienna sprawdzająca czy obrazek z Creditsami jest włączony, jej domyślna wartość to false.
    boolean isCredits = false;
    float time = 0;

    public MainMenu(VistulaGame vistulaGame) {
        game = vistulaGame;
        batch = new SpriteBatch();
//        Inicjacja obiektu font.
        font = new BitmapFont();
//        Inicjacja obiektu Stage.
        stage = new Stage();

        img = new Texture("creditsIMG.png");
//        Przypisanie dźwięku buttonClick.mp3 do zmiennej click.
        click = Gdx.audio.newSound(Gdx.files.internal("buttonClick.mp3"));
//        Przypisanie dźwięku menuMusic.mp3 do zmiennej bgMusic.
        bgMusic = Gdx.audio.newMusic(Gdx.files.internal("menuMusic.mp3"));
//        Ustawienie input processora na stage, dzięki temu obiekty wygenerowane przez stage będe będą przyjmowały input z dotyku, np tapnięcie ekranu, czy przeciąganie na ekranie
        Gdx.input.setInputProcessor(stage);
//        Wywołanie
        UICreate();
        UIElements();
    }

    public void UICreate() {
        bgMusic.setLooping(true);
        bgMusic.play();

        font.getData().setScale(2.4f);

        //Create a font
        skin = new Skin();
        skin.add("default", font);

        //Create a texture
        Pixmap pixmap = new Pixmap(Gdx.graphics.getWidth()/4, Gdx.graphics.getHeight()/10, Pixmap.Format.RGB888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("background",new Texture(pixmap));

        //Create a button style
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        Label.LabelStyle labelStyle = new Label.LabelStyle();
        textButtonStyle.up = skin.newDrawable("background", Color.GRAY);
        textButtonStyle.down = skin.newDrawable("background", Color.GRAY);
        textButtonStyle.checked = skin.newDrawable("background", Color.GRAY);
        textButtonStyle.over = skin.newDrawable("background", Color.LIGHT_GRAY);
        textButtonStyle.font = skin.getFont("default");
        labelStyle.font = skin.getFont("default");
        skin.add("default", textButtonStyle);
        skin.add("default", labelStyle);
    }

    public void UIElements() {
        TextButton newGameButton = new TextButton("New game", skin); // Use the initialized skin
        TextButton creditsButton = new TextButton("Credits", skin);
        TextButton exitButton = new TextButton("Exit", skin);
        Label titleLabel = new Label("Vistula Game", skin);

        titleLabel.setPosition(Gdx.graphics.getWidth()/2 - 125, Gdx.graphics.getHeight()*0.80f);
        newGameButton.setPosition(Gdx.graphics.getWidth()/2 - 150, Gdx.graphics.getHeight()*0.55f);
        creditsButton.setPosition(Gdx.graphics.getWidth()/2 - 150, Gdx.graphics.getHeight()*0.33f);
        exitButton.setPosition(Gdx.graphics.getWidth()/2 - 150, Gdx.graphics.getHeight()*0.11f);

        newGameButton.setSize(300,75);
        creditsButton.setSize(300,75);
        exitButton.setSize(300,75);

        newGameButton.getLabel().setFontScale(2);
        creditsButton.getLabel().setFontScale(2);
        exitButton.getLabel().setFontScale(2);

        titleLabel.setFontScale(3f);
        stage.addActor(titleLabel);
        stage.addActor(newGameButton);
        stage.addActor(creditsButton);
        stage.addActor(exitButton);

        newGameButton.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("New Game");
                Gdx.input.setInputProcessor(null);
                game.setScreen(new GameScreen(game));
                click.play();
                bgMusic.stop();
                return false;
            }
        });

        creditsButton.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("Credits");
                click.play();
                isCredits = true;
                return false;
            }
        });

        exitButton.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("Exit");
                click.play();
                bgMusic.stop();
                Gdx.app.exit();
                return false;
            }
        });
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.3f, 0.6f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (isCredits) {
            Gdx.input.setInputProcessor(null);
            batch.begin();
            batch.draw(img, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            batch.end();
            time +=delta;
            if (time > 0.4f && Gdx.input.justTouched()) {
                isCredits = false;
                time = 0;
                click.play();
            }
        }
        else{
            stage.act();
            stage.draw();
            Gdx.input.setInputProcessor(stage);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        batch.dispose();
        font.dispose();
        skin.dispose();
        click.dispose();
        bgMusic.dispose();
    }
}
