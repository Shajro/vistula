package com.libgdx.vistula.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.libgdx.vistula.VistulaGame;


//Implementacja klasy Screem
public class SplashScreen implements Screen {

//  Deklaracja zmiennych
    final VistulaGame game;
//  SpriteBatch używany jest do rysowania obiektów na ekranie
    SpriteBatch batch;
//    Texture używane jest do tworzenia tekstur
    Texture img;
//    Zmienna time używana bedzie do zapisywania w niej czasu jaki upłynął od wyświetlenia Screena "Splash Screen"
    float time = 0;


    public SplashScreen(final VistulaGame gam){
        game = gam;
    }

    @Override
//    Wszystkie instrukcje zawarte poniżej będą się wykonywały podczas pokazywania ekranu
    public void show() {
//        Utworzenie obiektu SpriteBatch, oraz przypisanie do niego zmiennej batch
        batch = new SpriteBatch();
//        Utworzenie tekstury z pliku "logo.png" i przypisanie do zmiennej img
        img = new Texture("logo.png");
    }

    @Override
//    Wsystkie instrukcje poniżej wykonywane są w czasie rzeczywistym, co każdą klatkę
    public void render(float delta) {
//    Ustawienie koloru czyszczenia ekranu
            Gdx.gl.glClearColor(1, 1, 1, 1);
//    Czyszczenie ekranu wybranym wcześniej kolorem, dzięki temu wszystko co jest renderowane, jest renderowane na czystym tle, bo niz zostaja informacje z poprzednich klatek
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//    Informacja dla SpriteBatch że od tego miejsca będą renderowane obrazy
        batch.begin();
//    Renderowanie tekstury img na kooredynatach (0,0), oraz o szerokości i długości takiej jaka jest rozdzielczosc ekranu.
//    W wypadku gdy obraz ma mniejszą rodzielczość, jest on rozciągany, jak większą to jest zmniejszany do wielkości ekranu.
//    Koordynaty w LibGDX określane są od lewego, dolnego rogu. (0,0) to lewy dolny róg.
        batch.draw(img, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
//    SpriteBatch kończy wyświetlanie obiektów
        batch.end();
//    Dodanie do zmiennej Time czasu delta
        time += delta;
//    Jeżeli zmienna time jest większa niż dwie sekundy, oraz ekran został dotkniety, wykonane zostaną polecenia w instrukcji
                if (time > 2f || Gdx.input.justTouched())
                {
//                    utworzenie ekranu MainMenu oraz ustawienie go jako głównego.
                        game.setScreen(new MainMenu(game));
                }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
//    Ta metoda została stworzona do tego, aby wszystkie obiekty które się w niej znajdują były usuwane podczas wyłączania aplikacji, tudzież innych akcji.
//    Ma to na celu optymalizacje wykorzystania pamięci
    public void dispose() {
        img.dispose();
        batch.dispose();
    }
}
