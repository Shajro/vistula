package com.libgdx.vistula.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.libgdx.vistula.Bob;
import com.libgdx.vistula.VistulaGame;
import java.util.Iterator;

/**
 * Created by Shajro on 23.10.2016.
 */

public class GameScreen implements Screen{

    final VistulaGame game;
    OrthographicCamera camera;
    SpriteBatch batch;
    TextureRegion ground;
    Music bgMusic;
    Sound deathSound;
    Texture barrierImage;
    private Bob bob;
    float offsetX;
    float time;
    Array<Rectangle> barriers;


    public GameScreen(VistulaGame vistulaGame) {
        game = vistulaGame;
        barrierImage = new Texture("obstacle.png");

        camera = new OrthographicCamera();
        batch = new SpriteBatch();
        ground = new TextureRegion(new Texture("ground.png"));

        bob = new Bob("playerImg.png");
        deathSound = Gdx.audio.newSound(Gdx.files.internal("gameOverSound.mp3"));

        offsetX = 0f;
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        barriers = new Array<Rectangle>();
        bgMusic = Gdx.audio.newMusic(Gdx.files.internal("gameMusic.mp3"));
        spawnBarrier();
    }

    @Override
    public void show() {
        camera.position.set(bob.getBobShape().x+ Gdx.graphics.getWidth()/2-64, Gdx.graphics.getHeight()/2, 0);
        batch.setProjectionMatrix(camera.combined);
        camera.update();
        bgMusic.setLooping(true);
        bgMusic.play();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        time += delta;
        bob.bobJump();
        camera.update();
        camera.position.set(bob.getBobShape().x+ Gdx.graphics.getWidth()/2-64, Gdx.graphics.getHeight()/2, 0);
        batch.setProjectionMatrix(camera.combined);

        batch.begin();
        renderGround(batch);

        for (Rectangle barrier : barriers) {
            batch.draw(barrierImage, barrier.x, barrier.y, barrier.width, barrier.height);
        }


        bob.runBob(batch);
        bob.deathScreen(batch);
        batch.end();

        if (time > 2 && bob.isDead == false)
        {
            spawnBarrier();
            time = 0;
//            System.out.println("spawn");
        }

        Iterator<Rectangle> iter = barriers.iterator();
        while (iter.hasNext()) {
            Rectangle barrier = iter.next();
            if (barrier.x < bob.getBobShape().getX() - 100) {
                System.out.println("Remove iteration");
                iter.remove();
            }
            if (barrier.overlaps(bob.getBobShape())) {

                iter.remove();
                deathSound.play();
                bob.isDead = true;
                barriers.clear();
                offsetX = 0f;
                System.out.println("collision");
            }
        }
//                        System.out.println(time);
    }



    private void renderGround(SpriteBatch batch) {
    if (camera.position.x - offsetX > 512){
        offsetX +=1024;
        }
        batch.draw(ground, offsetX-1024, 0);
        batch.draw(ground, offsetX, 0);
        if (bob.isDead && Gdx.input.isTouched()){
        offsetX=0f;
        }

    }

    private void spawnBarrier() {
        Rectangle barrier = new Rectangle();
        barrier.x = MathUtils.random(bob.getBobShape().getX() + 500, bob.getBobShape().getX() + 655);
        barrier.y = MathUtils.random(64f, 250f);
        barrier.width = 64;
        barrier.height = 64;
        barriers.add(barrier);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        bgMusic.dispose();
        deathSound.dispose();
        barrierImage.dispose();
        barriers.clear();
    }
}
