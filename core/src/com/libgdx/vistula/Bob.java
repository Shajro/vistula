package com.libgdx.vistula;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by Shajro on 31.10.2016.
 */

public class Bob extends Rectangle {
    private Rectangle bobShape;



    private float moveSpeed = 250;
    private float jumpSpeed = 0;

    private boolean isJumping;

    private Sound jump;
    private Texture texture, gameOverImage;

    public boolean isDead;

    float dt = Math.min(Gdx.graphics.getDeltaTime(), 1 / 60f);

    public Bob (String fileString){
        bobShape = new Rectangle();
        bobShape.x = 128;
        bobShape.y = 64;
        bobShape.setWidth(64);
        bobShape.setHeight(64);
        texture= new Texture(Gdx.files.internal(fileString));
        jump = Gdx.audio.newSound(Gdx.files.internal("jumpSound.mp3"));
        gameOverImage = new Texture("gameOverIMG.png");
        isDead = false;

    }

    public Texture getTexture() {
        return texture;
    }

    public void runBob(SpriteBatch batch) {
        if (bobShape.y > 230)
            jumpSpeed = -jumpSpeed;
        if (bobShape.y < 64){
            jumpSpeed = 0;
            bobShape.y = 64;
        }

        batch.draw(getTexture(), bobShape.x += dt * moveSpeed, bobShape.y +=jumpSpeed *dt, 64, 64);
    }

    public void bobJump() {
        if (Gdx.input.justTouched() || !isJumping) {
                isJumping = true;
                jumpSpeed = 200;
                jump.play();
        }
    }


    public void deathScreen(SpriteBatch batch) {
        if (isDead) {
            moveSpeed = 0;
            jumpSpeed = 0;
            batch.draw(gameOverImage, bobShape.x-64, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            if (Gdx.input.isTouched())
            {
                isDead = false;
                moveSpeed = 250;
                bobShape.setPosition(0,64);
            }
        }
    }

    public Rectangle getBobShape() {
        return bobShape;
    }

}
